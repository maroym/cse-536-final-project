THREADS 1
Reading File
Start traversing the tree
Compute time: 3.296008
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 3.634273
Result stored in result_omp_task.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 1.643932
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 1.739031
Result stored in result_omp_task.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 0.877978
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 1.063018
Result stored in result_omp_task.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 0.594900
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.690837
Result stored in result_omp_task.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 0.381661
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.504744
Result stored in result_omp_task.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 0.521932
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.573481
Result stored in result_omp_task.txt
