#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <omp.h>

#include "shared.h"
const char *algorithm_name = "bfs_omp_for";

void run(int num_threads, int no_of_nodes, bool* h_graph_mask, Node* h_graph_nodes, bool* h_graph_visited, int* h_cost, bool* h_updating_graph_mask, int *h_graph_edges)
{
    omp_set_num_threads(num_threads);

    bool keep_going;
    do
    {
        //if no thread changes this value then the loop stops
        keep_going=false;

        #pragma omp parallel for
        for(int tid = 0; tid < no_of_nodes; tid++ )
        {
            phase_one_single(tid, h_graph_mask, h_graph_nodes, h_graph_visited, h_cost, h_updating_graph_mask, h_graph_edges);
        }

        #pragma omp parallel for
        for(int tid=0; tid< no_of_nodes ; tid++ )
        {
            phase_two_single(tid, h_updating_graph_mask, h_graph_mask, h_graph_visited, &keep_going);
        }
    }
    while(keep_going);
}
