#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <sys/timeb.h>
#include <future>

using namespace std;

#include "shared.h"
const char *algorithm_name = "bfs_c++_async";

void run(int num_threads, int no_of_nodes, bool* h_graph_mask, Node* h_graph_nodes, bool* h_graph_visited, int* h_cost, bool* h_updating_graph_mask, int *h_graph_edges)
{
    std::future<void> futures[num_threads];

    bool keep_going;
    do
    {
        //if no thread changes this value then the loop stops
        keep_going=false;

        for (int i=0; i<num_threads; i++)
        {
            int start, end;
            dist(i, num_threads, no_of_nodes, &start, &end);

            futures[i] = std::async(std::launch::async, phase_one, start, end, h_graph_mask, h_graph_nodes, h_graph_visited, h_cost, h_updating_graph_mask, h_graph_edges);
        }
        for(int j=0;j<num_threads;++j)
        {
            futures[j].get();    
        }

        for (int i=0; i<num_threads; i++)
        {
            int start, end;
            dist(i, num_threads, no_of_nodes, &start, &end);
            futures[i] = std::async(std::launch::async, phase_two, start, end, &keep_going, h_graph_mask, h_graph_visited, h_updating_graph_mask);
        }
        for(int j=0;j<num_threads;++j)
        {
            futures[j].get();    
        }
    }
    while(keep_going);
}