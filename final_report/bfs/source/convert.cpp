/*
    This program converts the data format from the original Rodinia 
    text format to a binary form consisting of list of integers.
    This reduced the size and the load time of the data files.
*/
#include <stdio.h>

int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        printf("usage error: %s <input.txt> <output.dat>\n", argv[0]);
        return -1;
    }

    int value, result=0, count=0;
    FILE* fpText, *fpBinary;

    count = 0;
    fpText = fopen(argv[1],"r");
    fpBinary = fopen(argv[2],"w");

    while(!feof(fpText))
    {
        result = fscanf(fpText,"%d",&value);
        if (1 == result)
        {
            fwrite(&value,sizeof(int),1,fpBinary);
            count++;
        }
    }
    fclose(fpBinary);
    fclose(fpText);
    printf("Count: %d\n", count);

    return 0;    
}