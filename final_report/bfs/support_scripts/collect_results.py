import glob;

for fileName in glob.glob("*.txt"):
    with open(fileName) as file:
        for line in file.readlines():
            if line.startswith('DATA:'):
                print line.strip()[5:]
