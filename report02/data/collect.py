data = {}
for t in [1,2,4,8,16,32]:
    data[t] = {}
    for r in range(1,21):    
        data[t][r] = {}
        for a in ["bfs_omp_parallel","bfs_omp_task","bfs_cilkplus_cilk_for","bfs_cilkplus_cilk_spawn"]:
            data[t][r][a] = 0.0

for run in range(1,21):
    path = "result_{0:02}.txt".format(run)
    with open(path,'r') as file:
        for line in [line.strip() for line in file.readlines()]:
            if line.startswith("DATA:"):
                fields = line[5:].split(',')
                algorithm = fields[0]
                threads = fields[1]
                runtime = fields[3]

                data[int(threads)][run][algorithm] = runtime

for t in [1,2,4,8,16,32]:
    line = str(t)
    for r in range(1,21):
        for a in ["bfs_omp_parallel","bfs_omp_task","bfs_cilkplus_cilk_for","bfs_cilkplus_cilk_spawn"]:
            line += "," + data[t][r][a]
    print line



'''

data = {
    "1": {},
    "2": {},
    "4": {},
    "8": {},
    "16": {},
    "32": {}
}

for key in data:
    data[key] = {
        "bfs_omp_parallel": 0,
        "bfs_omp_task": 0,
        "bfs_cilkplus_cilk_for": 0,
        "bfs_cilkplus_cilk_spawn": 0
    }

# bfs_omp_parallel,2,16000000,3.200483,271471647

with open('results.csv', 'r') as file:
    for line in [line.strip() for line in file.readlines()]:
        fields = line.split(',')
        algorithm = fields[0]
        threasds = fields[1]
        runtime = fields[3]

        data[threasds][algorithm] = runtime

for k1 in data:
    for k2 in data[k1]:
        print k1 + ":" + k2 + ":" + str(data[k1][k2])

for th in ['1','2','4','8','16','32']:
    s = th
    for a in ["bfs_omp_parallel","bfs_omp_task","bfs_cilkplus_cilk_for","bfs_cilkplus_cilk_spawn":


]
'''