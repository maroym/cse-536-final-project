#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <sys/timeb.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>

/* read timer in second */
double read_timer() {
    struct timeb tm;
    ftime(&tm);
    return (double) tm.time + (double) tm.millitm / 1000.0;
}

/* read timer in ms */
double read_timer_ms() {
    struct timeb tm;
    ftime(&tm);
    return (double) tm.time * 1000.0 + (double) tm.millitm;
}

FILE *fp;

//Structure to hold a node information
struct Node
{
    int starting;
    int no_of_edges;
};

void BFSGraph(int argc, char** argv);

void Usage(int argc, char**argv)
{
    fprintf(stderr,"Usage: %s <num_threads> <input_file>\n", argv[0]);
}

void phase_one(int start, int end, bool &keep_going, bool *h_graph_mask, Node* h_graph_nodes, bool *h_graph_visited, int* h_cost, bool *h_updating_graph_mask, int* h_graph_edges);
void phase_two(int start, int end, bool &keep_going, bool *h_graph_mask, bool *h_graph_visited, bool *h_updating_graph_mask);

////////////////////////////////////////////////////////////////////////////////
// Main Program
////////////////////////////////////////////////////////////////////////////////
int main( int argc, char** argv)
{
    BFSGraph( argc, argv);
}

void dist(int tid, int num_tasks, int num_nodes, int *start, int *end ) {
    
    int Nt;
    int remain = num_nodes % num_tasks;
    int esize = num_nodes / num_tasks;

    if (tid < remain)
    {
        /* each of the first remain task has one more element */
        Nt = esize + 1;
        *start = Nt * tid;
    } else {
        Nt = esize;
        *start = esize * tid + remain;
    }
    *end = *start + Nt;
}

////////////////////////////////////////////////////////////////////////////////
//Apply BFS on a Graph using CUDA
////////////////////////////////////////////////////////////////////////////////
void BFSGraph( int argc, char** argv)
{
    int no_of_nodes = 0;
    int edge_list_size = 0;
    char *input_f;
    int num_threads;

    if(argc!=3)
    {
        Usage(argc, argv);
        exit(0);
    }

    num_threads = atoi(argv[1]);
    input_f = argv[2];

    printf("Reading File\n");
    //Read in Graph from a file
    fp = fopen(input_f,"r");
    if(!fp)
    {
        printf("Error Reading graph file\n");
        return;
    }

    char cilk_nworkers[3];
    sprintf(cilk_nworkers, "%d", num_threads);
    __cilkrts_set_param("nworkers", cilk_nworkers);

    int source = 0;

    fscanf(fp,"%d",&no_of_nodes);

    // allocate host memory
    Node* h_graph_nodes = (Node*) malloc(sizeof(Node)*no_of_nodes);
    bool *h_graph_mask = (bool*) malloc(sizeof(bool)*no_of_nodes);
    bool *h_updating_graph_mask = (bool*) malloc(sizeof(bool)*no_of_nodes);
    bool *h_graph_visited = (bool*) malloc(sizeof(bool)*no_of_nodes);

    int start, edgeno;
    // initalize the memory
    for( unsigned int i = 0; i < no_of_nodes; i++)
    {
        fscanf(fp,"%d %d",&start,&edgeno);
        h_graph_nodes[i].starting = start;
        h_graph_nodes[i].no_of_edges = edgeno;
        h_graph_mask[i]=false;
        h_updating_graph_mask[i]=false;
        h_graph_visited[i]=false;
    }

    //read the source node from the file
    fscanf(fp,"%d",&source);
    // source=0; //tesing code line

    //set the source node as true in the mask
    h_graph_mask[source]=true;
    h_graph_visited[source]=true;

    fscanf(fp,"%d",&edge_list_size);

    int id,cost;
    int* h_graph_edges = (int*) malloc(sizeof(int)*edge_list_size);
    for(int i=0; i < edge_list_size ; i++)
    {
        fscanf(fp,"%d",&id);
        fscanf(fp,"%d",&cost);
        h_graph_edges[i] = id;
    }

    if(fp)
    {
        fclose(fp);
    }

    // allocate mem for the result on host side
    int* h_cost = (int*) malloc( sizeof(int)*no_of_nodes);
    for(int i=0;i<no_of_nodes;i++)
    {
        h_cost[i]=-1;
    }
    h_cost[source]=0;

    printf("Start traversing the tree\n");

    double start_time = read_timer();
    bool keep_going;
    do
    {
        //if no thread changes this value then the loop stops
        keep_going=false;

        for (int i=0; i<num_threads; i++)
        {
            int start, end;
            dist(i, num_threads, no_of_nodes, &start, &end);
            cilk_spawn phase_one(start, end, keep_going, h_graph_mask, h_graph_nodes, h_graph_visited, h_cost, h_updating_graph_mask, h_graph_edges);
        }
        cilk_sync;

        for (int i=0; i<num_threads; i++)
        {
            int start, end;
            dist(i, num_threads, no_of_nodes, &start, &end);
            cilk_spawn phase_two(start, end, keep_going, h_graph_mask, h_graph_visited, h_updating_graph_mask);
        }
        cilk_sync;
    }
    while(keep_going);

    double end_time = read_timer();
    printf("Compute time: %lf\n", (end_time - start_time));

    //Store the result into a file
    FILE *fpo = fopen("result_cilkplus_cilk_spawn.txt","w");
    for(int i=0;i<no_of_nodes;i++)
    {
        fprintf(fpo,"%d) cost:%d\n",i,h_cost[i]);
    }
    
    fseek(fpo, 0L, SEEK_END);
    printf("DATA:bfs_cilkplus_cilk_spawn,%d,%d,%lf,%ld\n",num_threads,no_of_nodes,(end_time - start_time),ftell(fpo));

    fclose(fpo);
    printf("Result stored in result_cilkplus_cilk_spawn.txt\n");

    // cleanup memory
    free(h_graph_nodes);
    free(h_graph_edges);
    free(h_graph_mask);
    free(h_updating_graph_mask);
    free(h_graph_visited);
    free(h_cost);
}

void phase_one(int start, int end, bool &keep_going, bool *h_graph_mask, Node* h_graph_nodes, bool *h_graph_visited, int* h_cost, bool *h_updating_graph_mask, int* h_graph_edges)
{
    for(int n = start; n < end; n++ )
    {
        if (h_graph_mask[n] == true)
        {
            h_graph_mask[n]=false;
            for(int e=h_graph_nodes[n].starting; e<(h_graph_nodes[n].no_of_edges + h_graph_nodes[n].starting); e++)
            {
                int id = h_graph_edges[e];
                if(!h_graph_visited[id])
                {
                    h_cost[id]=h_cost[n]+1;
                    h_updating_graph_mask[id]=true;
                }
            }
        }
    }
}

void phase_two(int start, int end, bool &keep_going, bool *h_graph_mask, bool *h_graph_visited, bool *h_updating_graph_mask)
{
    for(int n=start; n<end; n++ )
    {
        if (h_updating_graph_mask[n] == true)
        {
            h_graph_mask[n]=true;
            h_graph_visited[n]=true;
            keep_going=true;
            h_updating_graph_mask[n]=false;
        }
    }
}