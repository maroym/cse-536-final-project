THREADS 1
Reading File
Start traversing the tree
Compute time: 8.593000
DATA:bfs_omp_for,1,16000000,8.593000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 8.437000
DATA:bfs_omp_task,1,16000000,8.437000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 8.449000
DATA:bfs_cilk_for,1,16000000,8.449000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 8.672000
DATA:bfs_cilk_spawn,1,16000000,8.672000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 8.203000
DATA:bfs_c++_thread,1,16000000,8.203000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 8.228000
DATA:bfs_c++_async,1,16000000,8.228000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 4.312000
DATA:bfs_omp_for,2,16000000,4.312000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 4.217000
DATA:bfs_omp_task,2,16000000,4.217000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 4.750000
DATA:bfs_cilk_for,2,16000000,4.750000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 4.452000
DATA:bfs_cilk_spawn,2,16000000,4.452000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 4.753000
DATA:bfs_c++_thread,2,16000000,4.753000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 4.810000
DATA:bfs_c++_async,2,16000000,4.810000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 2.667000
DATA:bfs_omp_for,4,16000000,2.667000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 2.489000
DATA:bfs_omp_task,4,16000000,2.489000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.360000
DATA:bfs_cilk_for,4,16000000,2.360000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 2.785000
DATA:bfs_cilk_spawn,4,16000000,2.785000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.634000
DATA:bfs_c++_thread,4,16000000,2.634000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.618000
DATA:bfs_c++_async,4,16000000,2.618000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 2.200000
DATA:bfs_omp_for,8,16000000,2.200000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.615000
DATA:bfs_omp_task,8,16000000,1.615000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.604000
DATA:bfs_cilk_for,8,16000000,1.604000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.727000
DATA:bfs_cilk_spawn,8,16000000,1.727000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.680000
DATA:bfs_c++_thread,8,16000000,1.680000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.682000
DATA:bfs_c++_async,8,16000000,1.682000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 1.224000
DATA:bfs_omp_for,16,16000000,1.224000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.128000
DATA:bfs_omp_task,16,16000000,1.128000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.134000
DATA:bfs_cilk_for,16,16000000,1.134000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.240000
DATA:bfs_cilk_spawn,16,16000000,1.240000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.152000
DATA:bfs_c++_thread,16,16000000,1.152000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.183000
DATA:bfs_c++_async,16,16000000,1.183000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 1.415000
DATA:bfs_omp_for,32,16000000,1.415000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.859000
DATA:bfs_omp_task,32,16000000,0.859000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.998000
DATA:bfs_cilk_for,32,16000000,0.998000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.102000
DATA:bfs_cilk_spawn,32,16000000,1.102000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.907000
DATA:bfs_c++_thread,32,16000000,0.907000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.895000
DATA:bfs_c++_async,32,16000000,0.895000,271471647
Result stored in result_bfs_c++_async.txt
