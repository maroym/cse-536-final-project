THREADS 1
Reading File
Start traversing the tree
Compute time: 8.107000
DATA:bfs_omp_for,1,16000000,8.107000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 7.955000
DATA:bfs_omp_task,1,16000000,7.955000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 8.083000
DATA:bfs_cilk_for,1,16000000,8.083000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 8.205000
DATA:bfs_cilk_spawn,1,16000000,8.205000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 7.724000
DATA:bfs_c++_thread,1,16000000,7.724000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 7.789000
DATA:bfs_c++_async,1,16000000,7.789000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 5.129000
DATA:bfs_omp_for,2,16000000,5.129000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 4.634000
DATA:bfs_omp_task,2,16000000,4.634000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 4.019000
DATA:bfs_cilk_for,2,16000000,4.019000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 4.449000
DATA:bfs_cilk_spawn,2,16000000,4.449000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 4.350000
DATA:bfs_c++_thread,2,16000000,4.350000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 4.604000
DATA:bfs_c++_async,2,16000000,4.604000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 2.864000
DATA:bfs_omp_for,4,16000000,2.864000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 2.493000
DATA:bfs_omp_task,4,16000000,2.493000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.974000
DATA:bfs_cilk_for,4,16000000,2.974000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 2.465000
DATA:bfs_cilk_spawn,4,16000000,2.465000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.537000
DATA:bfs_c++_thread,4,16000000,2.537000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.483000
DATA:bfs_c++_async,4,16000000,2.483000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 1.640000
DATA:bfs_omp_for,8,16000000,1.640000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.610000
DATA:bfs_omp_task,8,16000000,1.610000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.190000
DATA:bfs_cilk_for,8,16000000,2.190000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.755000
DATA:bfs_cilk_spawn,8,16000000,1.755000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.682000
DATA:bfs_c++_thread,8,16000000,1.682000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.688000
DATA:bfs_c++_async,8,16000000,1.688000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 1.729000
DATA:bfs_omp_for,16,16000000,1.729000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.117000
DATA:bfs_omp_task,16,16000000,1.117000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.123000
DATA:bfs_cilk_for,16,16000000,1.123000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.251000
DATA:bfs_cilk_spawn,16,16000000,1.251000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.164000
DATA:bfs_c++_thread,16,16000000,1.164000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.159000
DATA:bfs_c++_async,16,16000000,1.159000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 0.913000
DATA:bfs_omp_for,32,16000000,0.913000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.919000
DATA:bfs_omp_task,32,16000000,0.919000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.981000
DATA:bfs_cilk_for,32,16000000,0.981000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.092000
DATA:bfs_cilk_spawn,32,16000000,1.092000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.908000
DATA:bfs_c++_thread,32,16000000,0.908000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.916000
DATA:bfs_c++_async,32,16000000,0.916000,271471647
Result stored in result_bfs_c++_async.txt
