THREADS 1
Reading File
Start traversing the tree
Compute time: 8.131000
DATA:bfs_omp_for,1,16000000,8.131000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 7.829000
DATA:bfs_omp_task,1,16000000,7.829000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 8.127000
DATA:bfs_cilk_for,1,16000000,8.127000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 8.281000
DATA:bfs_cilk_spawn,1,16000000,8.281000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 7.781000
DATA:bfs_c++_thread,1,16000000,7.781000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 7.754000
DATA:bfs_c++_async,1,16000000,7.754000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 4.271000
DATA:bfs_omp_for,2,16000000,4.271000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 3.977000
DATA:bfs_omp_task,2,16000000,3.977000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 4.100000
DATA:bfs_cilk_for,2,16000000,4.100000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 4.947000
DATA:bfs_cilk_spawn,2,16000000,4.947000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 4.653000
DATA:bfs_c++_thread,2,16000000,4.653000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 4.532000
DATA:bfs_c++_async,2,16000000,4.532000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 2.482000
DATA:bfs_omp_for,4,16000000,2.482000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 2.437000
DATA:bfs_omp_task,4,16000000,2.437000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.501000
DATA:bfs_cilk_for,4,16000000,2.501000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 2.685000
DATA:bfs_cilk_spawn,4,16000000,2.685000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.595000
DATA:bfs_c++_thread,4,16000000,2.595000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.641000
DATA:bfs_c++_async,4,16000000,2.641000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 2.064000
DATA:bfs_omp_for,8,16000000,2.064000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.665000
DATA:bfs_omp_task,8,16000000,1.665000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.112000
DATA:bfs_cilk_for,8,16000000,2.112000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.799000
DATA:bfs_cilk_spawn,8,16000000,1.799000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.680000
DATA:bfs_c++_thread,8,16000000,1.680000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.719000
DATA:bfs_c++_async,8,16000000,1.719000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 1.734000
DATA:bfs_omp_for,16,16000000,1.734000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.217000
DATA:bfs_omp_task,16,16000000,1.217000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.237000
DATA:bfs_cilk_for,16,16000000,1.237000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.229000
DATA:bfs_cilk_spawn,16,16000000,1.229000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.148000
DATA:bfs_c++_thread,16,16000000,1.148000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.196000
DATA:bfs_c++_async,16,16000000,1.196000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 0.876000
DATA:bfs_omp_for,32,16000000,0.876000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.919000
DATA:bfs_omp_task,32,16000000,0.919000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.961000
DATA:bfs_cilk_for,32,16000000,0.961000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.035000
DATA:bfs_cilk_spawn,32,16000000,1.035000,271593254
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.935000
DATA:bfs_c++_thread,32,16000000,0.935000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.906000
DATA:bfs_c++_async,32,16000000,0.906000,271471647
Result stored in result_bfs_c++_async.txt
