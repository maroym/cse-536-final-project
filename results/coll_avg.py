import glob;
import sys

def avg(l):
    return sum(l) / float(len(l))

data = dict()
for fileName in glob.glob("result_??.txt"):
    with open(fileName) as file:
        for line in file.readlines():
            if line.startswith('DATA:'):
                line = line.strip()[5:]
                parts = line.strip().split(',')
                key = ','.join(parts[0:3])
                if not key in data:
                    data[key] = []
                data[key].append(float(parts[3]))

for key in data:
    print key + ',' + str(avg(data[key]))
