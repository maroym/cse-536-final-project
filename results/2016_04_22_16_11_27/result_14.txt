THREADS 1
Reading File
Start traversing the tree
Compute time: 6.759000
DATA:bfs_omp_for,1,16000000,6.759000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 6.214000
DATA:bfs_omp_task,1,16000000,6.214000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 6.434000
DATA:bfs_cilk_for,1,16000000,6.434000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 6.870000
DATA:bfs_cilk_spawn,1,16000000,6.870000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 6.601000
DATA:bfs_c++_thread,1,16000000,6.601000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 7.406000
DATA:bfs_c++_async,1,16000000,7.406000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 3.500000
DATA:bfs_omp_for,2,16000000,3.500000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 3.429000
DATA:bfs_omp_task,2,16000000,3.429000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 4.531000
DATA:bfs_cilk_for,2,16000000,4.531000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 4.584000
DATA:bfs_cilk_spawn,2,16000000,4.584000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 4.099000
DATA:bfs_c++_thread,2,16000000,4.099000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 4.749000
DATA:bfs_c++_async,2,16000000,4.749000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 2.554000
DATA:bfs_omp_for,4,16000000,2.554000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 2.829000
DATA:bfs_omp_task,4,16000000,2.829000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.434000
DATA:bfs_cilk_for,4,16000000,2.434000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 2.772000
DATA:bfs_cilk_spawn,4,16000000,2.772000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.592000
DATA:bfs_c++_thread,4,16000000,2.592000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.573000
DATA:bfs_c++_async,4,16000000,2.573000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 1.688000
DATA:bfs_omp_for,8,16000000,1.688000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.633000
DATA:bfs_omp_task,8,16000000,1.633000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.650000
DATA:bfs_cilk_for,8,16000000,1.650000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.518000
DATA:bfs_cilk_spawn,8,16000000,1.518000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.661000
DATA:bfs_c++_thread,8,16000000,1.661000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.701000
DATA:bfs_c++_async,8,16000000,1.701000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 1.698000
DATA:bfs_omp_for,16,16000000,1.698000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.182000
DATA:bfs_omp_task,16,16000000,1.182000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.640000
DATA:bfs_cilk_for,16,16000000,1.640000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.311000
DATA:bfs_cilk_spawn,16,16000000,1.311000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.234000
DATA:bfs_c++_thread,16,16000000,1.234000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.283000
DATA:bfs_c++_async,16,16000000,1.283000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 1.470000
DATA:bfs_omp_for,32,16000000,1.470000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.993000
DATA:bfs_omp_task,32,16000000,0.993000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.985000
DATA:bfs_cilk_for,32,16000000,0.985000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.060000
DATA:bfs_cilk_spawn,32,16000000,1.060000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.984000
DATA:bfs_c++_thread,32,16000000,0.984000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.015000
DATA:bfs_c++_async,32,16000000,1.015000,271471647
Result stored in result_bfs_c++_async.txt
