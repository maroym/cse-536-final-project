THREADS 1
Reading File
Start traversing the tree
Compute time: 6.613000
DATA:bfs_omp_for,1,16000000,6.613000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 5.962000
DATA:bfs_omp_task,1,16000000,5.962000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 6.752000
DATA:bfs_cilk_for,1,16000000,6.752000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 6.901000
DATA:bfs_cilk_spawn,1,16000000,6.901000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 7.581000
DATA:bfs_c++_thread,1,16000000,7.581000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 6.771000
DATA:bfs_c++_async,1,16000000,6.771000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 3.439000
DATA:bfs_omp_for,2,16000000,3.439000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 4.518000
DATA:bfs_omp_task,2,16000000,4.518000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 3.224000
DATA:bfs_cilk_for,2,16000000,3.224000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 3.626000
DATA:bfs_cilk_spawn,2,16000000,3.626000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 4.111000
DATA:bfs_c++_thread,2,16000000,4.111000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 4.742000
DATA:bfs_c++_async,2,16000000,4.742000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 2.922000
DATA:bfs_omp_for,4,16000000,2.922000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 2.627000
DATA:bfs_omp_task,4,16000000,2.627000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.519000
DATA:bfs_cilk_for,4,16000000,2.519000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 2.286000
DATA:bfs_cilk_spawn,4,16000000,2.286000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.616000
DATA:bfs_c++_thread,4,16000000,2.616000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.808000
DATA:bfs_c++_async,4,16000000,2.808000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 1.865000
DATA:bfs_omp_for,8,16000000,1.865000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.632000
DATA:bfs_omp_task,8,16000000,1.632000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.503000
DATA:bfs_cilk_for,8,16000000,1.503000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.870000
DATA:bfs_cilk_spawn,8,16000000,1.870000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.756000
DATA:bfs_c++_thread,8,16000000,1.756000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.649000
DATA:bfs_c++_async,8,16000000,1.649000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 1.245000
DATA:bfs_omp_for,16,16000000,1.245000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.207000
DATA:bfs_omp_task,16,16000000,1.207000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.204000
DATA:bfs_cilk_for,16,16000000,1.204000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.356000
DATA:bfs_cilk_spawn,16,16000000,1.356000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.232000
DATA:bfs_c++_thread,16,16000000,1.232000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.274000
DATA:bfs_c++_async,16,16000000,1.274000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 1.021000
DATA:bfs_omp_for,32,16000000,1.021000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.918000
DATA:bfs_omp_task,32,16000000,0.918000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.965000
DATA:bfs_cilk_for,32,16000000,0.965000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.131000
DATA:bfs_cilk_spawn,32,16000000,1.131000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.984000
DATA:bfs_c++_thread,32,16000000,0.984000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.020000
DATA:bfs_c++_async,32,16000000,1.020000,271471647
Result stored in result_bfs_c++_async.txt
