THREADS 1
Reading File
Start traversing the tree
Compute time: 6.464000
DATA:bfs_omp_for,1,16000000,6.464000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 6.242000
DATA:bfs_omp_task,1,16000000,6.242000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 6.571000
DATA:bfs_cilk_for,1,16000000,6.571000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 7.024000
DATA:bfs_cilk_spawn,1,16000000,7.024000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 6.105000
DATA:bfs_c++_thread,1,16000000,6.105000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 6.187000
DATA:bfs_c++_async,1,16000000,6.187000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 3.552000
DATA:bfs_omp_for,2,16000000,3.552000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 3.447000
DATA:bfs_omp_task,2,16000000,3.447000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 3.258000
DATA:bfs_cilk_for,2,16000000,3.258000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 3.749000
DATA:bfs_cilk_spawn,2,16000000,3.749000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 4.641000
DATA:bfs_c++_thread,2,16000000,4.641000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 4.700000
DATA:bfs_c++_async,2,16000000,4.700000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 2.853000
DATA:bfs_omp_for,4,16000000,2.853000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 2.516000
DATA:bfs_omp_task,4,16000000,2.516000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.273000
DATA:bfs_cilk_for,4,16000000,2.273000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 2.909000
DATA:bfs_cilk_spawn,4,16000000,2.909000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.544000
DATA:bfs_c++_thread,4,16000000,2.544000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.555000
DATA:bfs_c++_async,4,16000000,2.555000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 1.643000
DATA:bfs_omp_for,8,16000000,1.643000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.739000
DATA:bfs_omp_task,8,16000000,1.739000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.107000
DATA:bfs_cilk_for,8,16000000,2.107000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.296000
DATA:bfs_cilk_spawn,8,16000000,1.296000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.802000
DATA:bfs_c++_thread,8,16000000,1.802000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.677000
DATA:bfs_c++_async,8,16000000,1.677000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 1.692000
DATA:bfs_omp_for,16,16000000,1.692000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.231000
DATA:bfs_omp_task,16,16000000,1.231000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.230000
DATA:bfs_cilk_for,16,16000000,1.230000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.281000
DATA:bfs_cilk_spawn,16,16000000,1.281000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.220000
DATA:bfs_c++_thread,16,16000000,1.220000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.217000
DATA:bfs_c++_async,16,16000000,1.217000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 1.509000
DATA:bfs_omp_for,32,16000000,1.509000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.981000
DATA:bfs_omp_task,32,16000000,0.981000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.949000
DATA:bfs_cilk_for,32,16000000,0.949000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.121000
DATA:bfs_cilk_spawn,32,16000000,1.121000,271583025
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.961000
DATA:bfs_c++_thread,32,16000000,0.961000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.996000
DATA:bfs_c++_async,32,16000000,0.996000,271471647
Result stored in result_bfs_c++_async.txt
