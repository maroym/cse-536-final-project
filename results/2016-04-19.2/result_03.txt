THREADS 1
Reading File
Start traversing the tree
Compute time: 1.480000
DATA:bfs_omp_for,1,4000000,1.480000,63640867
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.379000
DATA:bfs_omp_task,1,4000000,1.379000,63640867
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.414000
DATA:bfs_cilk_for,1,4000000,1.414000,63640867
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.421000
DATA:bfs_cilk_spawn,1,4000000,1.421000,63640867
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.394000
DATA:bfs_c++_thread,1,4000000,1.394000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.692000
DATA:bfs_c++_async,1,4000000,1.692000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 0.812000
DATA:bfs_omp_for,2,4000000,0.812000,63640867
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.670000
DATA:bfs_omp_task,2,4000000,0.670000,63640867
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.718000
DATA:bfs_cilk_for,2,4000000,0.718000,63640867
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.719000
DATA:bfs_cilk_spawn,2,4000000,0.719000,63640867
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.985000
DATA:bfs_c++_thread,2,4000000,0.985000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.031000
DATA:bfs_c++_async,2,4000000,1.031000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 0.570000
DATA:bfs_omp_for,4,4000000,0.570000,63640867
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.565000
DATA:bfs_omp_task,4,4000000,0.565000,63640867
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.578000
DATA:bfs_cilk_for,4,4000000,0.578000,63640867
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.591000
DATA:bfs_cilk_spawn,4,4000000,0.591000,63640867
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.575000
DATA:bfs_c++_thread,4,4000000,0.575000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.571000
DATA:bfs_c++_async,4,4000000,0.571000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 0.500000
DATA:bfs_omp_for,8,4000000,0.500000,63640867
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.390000
DATA:bfs_omp_task,8,4000000,0.390000,63640867
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.479000
DATA:bfs_cilk_for,8,4000000,0.479000,63640867
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.489000
DATA:bfs_cilk_spawn,8,4000000,0.489000,63640867
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.390000
DATA:bfs_c++_thread,8,4000000,0.390000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.404000
DATA:bfs_c++_async,8,4000000,0.404000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 0.276000
DATA:bfs_omp_for,16,4000000,0.276000,63640867
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.266000
DATA:bfs_omp_task,16,4000000,0.266000,63640867
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.407000
DATA:bfs_cilk_for,16,4000000,0.407000,63640867
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.361000
DATA:bfs_cilk_spawn,16,4000000,0.361000,63640867
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.277000
DATA:bfs_c++_thread,16,4000000,0.277000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.280000
DATA:bfs_c++_async,16,4000000,0.280000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 0.324000
DATA:bfs_omp_for,32,4000000,0.324000,63640867
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.222000
DATA:bfs_omp_task,32,4000000,0.222000,63640867
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.250000
DATA:bfs_cilk_for,32,4000000,0.250000,63640867
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.336000
DATA:bfs_cilk_spawn,32,4000000,0.336000,63705588
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.237000
DATA:bfs_c++_thread,32,4000000,0.237000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.229000
DATA:bfs_c++_async,32,4000000,0.229000,63640867
Result stored in result_bfs_c++_async.txt
