THREADS 1
Reading File
Start traversing the tree
Compute time: 1.443000
DATA:bfs_omp_for,1,4000000,1.443000,63640867
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.399000
DATA:bfs_omp_task,1,4000000,1.399000,63640867
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.505000
DATA:bfs_cilk_for,1,4000000,1.505000,63640867
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.481000
DATA:bfs_cilk_spawn,1,4000000,1.481000,63640867
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.699000
DATA:bfs_c++_thread,1,4000000,1.699000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.360000
DATA:bfs_c++_async,1,4000000,1.360000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 0.845000
DATA:bfs_omp_for,2,4000000,0.845000,63640867
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.688000
DATA:bfs_omp_task,2,4000000,0.688000,63640867
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.763000
DATA:bfs_cilk_for,2,4000000,0.763000,63640867
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.004000
DATA:bfs_cilk_spawn,2,4000000,1.004000,63640867
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.010000
DATA:bfs_c++_thread,2,4000000,1.010000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.029000
DATA:bfs_c++_async,2,4000000,1.029000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 0.653000
DATA:bfs_omp_for,4,4000000,0.653000,63640867
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.567000
DATA:bfs_omp_task,4,4000000,0.567000,63640867
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.498000
DATA:bfs_cilk_for,4,4000000,0.498000,63640867
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.686000
DATA:bfs_cilk_spawn,4,4000000,0.686000,63640867
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.570000
DATA:bfs_c++_thread,4,4000000,0.570000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.580000
DATA:bfs_c++_async,4,4000000,0.580000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 0.411000
DATA:bfs_omp_for,8,4000000,0.411000,63640867
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.383000
DATA:bfs_omp_task,8,4000000,0.383000,63640867
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.358000
DATA:bfs_cilk_for,8,4000000,0.358000,63640867
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.466000
DATA:bfs_cilk_spawn,8,4000000,0.466000,63640867
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.383000
DATA:bfs_c++_thread,8,4000000,0.383000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.379000
DATA:bfs_c++_async,8,4000000,0.379000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 0.399000
DATA:bfs_omp_for,16,4000000,0.399000,63640867
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.259000
DATA:bfs_omp_task,16,4000000,0.259000,63640867
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.293000
DATA:bfs_cilk_for,16,4000000,0.293000,63640867
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.356000
DATA:bfs_cilk_spawn,16,4000000,0.356000,63640867
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.291000
DATA:bfs_c++_thread,16,4000000,0.291000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.284000
DATA:bfs_c++_async,16,4000000,0.284000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 0.320000
DATA:bfs_omp_for,32,4000000,0.320000,63640867
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.224000
DATA:bfs_omp_task,32,4000000,0.224000,63640867
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.254000
DATA:bfs_cilk_for,32,4000000,0.254000,63640867
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.278000
DATA:bfs_cilk_spawn,32,4000000,0.278000,63666438
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.229000
DATA:bfs_c++_thread,32,4000000,0.229000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.225000
DATA:bfs_c++_async,32,4000000,0.225000,63640867
Result stored in result_bfs_c++_async.txt
