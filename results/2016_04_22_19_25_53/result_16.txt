THREADS 1
Reading File
Start traversing the tree
Compute time: 7.508000
DATA:bfs_omp_for,1,16000000,7.508000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 7.250000
DATA:bfs_omp_task,1,16000000,7.250000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 7.237000
DATA:bfs_cilk_for,1,16000000,7.237000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 7.898000
DATA:bfs_cilk_spawn,1,16000000,7.898000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 7.430000
DATA:bfs_c++_thread,1,16000000,7.430000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 7.421000
DATA:bfs_c++_async,1,16000000,7.421000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 4.144000
DATA:bfs_omp_for,2,16000000,4.144000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 4.466000
DATA:bfs_omp_task,2,16000000,4.466000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 3.939000
DATA:bfs_cilk_for,2,16000000,3.939000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 4.409000
DATA:bfs_cilk_spawn,2,16000000,4.409000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 4.037000
DATA:bfs_c++_thread,2,16000000,4.037000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 4.182000
DATA:bfs_c++_async,2,16000000,4.182000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 2.792000
DATA:bfs_omp_for,4,16000000,2.792000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 2.474000
DATA:bfs_omp_task,4,16000000,2.474000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.478000
DATA:bfs_cilk_for,4,16000000,2.478000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 2.687000
DATA:bfs_cilk_spawn,4,16000000,2.687000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.588000
DATA:bfs_c++_thread,4,16000000,2.588000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.441000
DATA:bfs_c++_async,4,16000000,2.441000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 1.637000
DATA:bfs_omp_for,8,16000000,1.637000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.592000
DATA:bfs_omp_task,8,16000000,1.592000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.573000
DATA:bfs_cilk_for,8,16000000,1.573000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.840000
DATA:bfs_cilk_spawn,8,16000000,1.840000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.571000
DATA:bfs_c++_thread,8,16000000,1.571000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.657000
DATA:bfs_c++_async,8,16000000,1.657000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 1.146000
DATA:bfs_omp_for,16,16000000,1.146000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.158000
DATA:bfs_omp_task,16,16000000,1.158000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.169000
DATA:bfs_cilk_for,16,16000000,1.169000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.169000
DATA:bfs_cilk_spawn,16,16000000,1.169000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.172000
DATA:bfs_c++_thread,16,16000000,1.172000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.186000
DATA:bfs_c++_async,16,16000000,1.186000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 0.941000
DATA:bfs_omp_for,32,16000000,0.941000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.926000
DATA:bfs_omp_task,32,16000000,0.926000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.963000
DATA:bfs_cilk_for,32,16000000,0.963000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.976000
DATA:bfs_cilk_spawn,32,16000000,0.976000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.934000
DATA:bfs_c++_thread,32,16000000,0.934000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.937000
DATA:bfs_c++_async,32,16000000,0.937000,271471647
Result stored in result_bfs_c++_async.txt
