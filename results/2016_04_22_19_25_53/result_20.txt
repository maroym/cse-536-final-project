THREADS 1
Reading File
Start traversing the tree
Compute time: 7.193000
DATA:bfs_omp_for,1,16000000,7.193000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 7.393000
DATA:bfs_omp_task,1,16000000,7.393000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 7.563000
DATA:bfs_cilk_for,1,16000000,7.563000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 7.888000
DATA:bfs_cilk_spawn,1,16000000,7.888000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 7.804000
DATA:bfs_c++_thread,1,16000000,7.804000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 7.171000
DATA:bfs_c++_async,1,16000000,7.171000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 4.615000
DATA:bfs_omp_for,2,16000000,4.615000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 4.153000
DATA:bfs_omp_task,2,16000000,4.153000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 4.107000
DATA:bfs_cilk_for,2,16000000,4.107000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 4.282000
DATA:bfs_cilk_spawn,2,16000000,4.282000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 4.462000
DATA:bfs_c++_thread,2,16000000,4.462000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 4.570000
DATA:bfs_c++_async,2,16000000,4.570000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 2.544000
DATA:bfs_omp_for,4,16000000,2.544000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 2.314000
DATA:bfs_omp_task,4,16000000,2.314000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.678000
DATA:bfs_cilk_for,4,16000000,2.678000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 2.713000
DATA:bfs_cilk_spawn,4,16000000,2.713000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.446000
DATA:bfs_c++_thread,4,16000000,2.446000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.431000
DATA:bfs_c++_async,4,16000000,2.431000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 2.019000
DATA:bfs_omp_for,8,16000000,2.019000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.625000
DATA:bfs_omp_task,8,16000000,1.625000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.613000
DATA:bfs_cilk_for,8,16000000,1.613000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.652000
DATA:bfs_cilk_spawn,8,16000000,1.652000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.601000
DATA:bfs_c++_thread,8,16000000,1.601000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.563000
DATA:bfs_c++_async,8,16000000,1.563000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 1.781000
DATA:bfs_omp_for,16,16000000,1.781000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.146000
DATA:bfs_omp_task,16,16000000,1.146000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.227000
DATA:bfs_cilk_for,16,16000000,1.227000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.357000
DATA:bfs_cilk_spawn,16,16000000,1.357000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.207000
DATA:bfs_c++_thread,16,16000000,1.207000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.158000
DATA:bfs_c++_async,16,16000000,1.158000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 1.440000
DATA:bfs_omp_for,32,16000000,1.440000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.952000
DATA:bfs_omp_task,32,16000000,0.952000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.944000
DATA:bfs_cilk_for,32,16000000,0.944000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.102000
DATA:bfs_cilk_spawn,32,16000000,1.102000,271591948
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.937000
DATA:bfs_c++_thread,32,16000000,0.937000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.931000
DATA:bfs_c++_async,32,16000000,0.931000,271471647
Result stored in result_bfs_c++_async.txt
