THREADS 1
Reading File
Start traversing the tree
Compute time: 8.564000
DATA:bfs_omp_for,1,16000000,8.564000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 8.256000
DATA:bfs_omp_task,1,16000000,8.256000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 8.336000
DATA:bfs_cilk_for,1,16000000,8.336000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 8.445000
DATA:bfs_cilk_spawn,1,16000000,8.445000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 8.467000
DATA:bfs_c++_thread,1,16000000,8.467000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 8.645000
DATA:bfs_c++_async,1,16000000,8.645000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 4.402000
DATA:bfs_omp_for,2,16000000,4.402000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 4.109000
DATA:bfs_omp_task,2,16000000,4.109000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 4.277000
DATA:bfs_cilk_for,2,16000000,4.277000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 4.842000
DATA:bfs_cilk_spawn,2,16000000,4.842000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 5.096000
DATA:bfs_c++_thread,2,16000000,5.096000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 4.740000
DATA:bfs_c++_async,2,16000000,4.740000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 2.943000
DATA:bfs_omp_for,4,16000000,2.943000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 2.587000
DATA:bfs_omp_task,4,16000000,2.587000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.690000
DATA:bfs_cilk_for,4,16000000,2.690000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 2.531000
DATA:bfs_cilk_spawn,4,16000000,2.531000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.677000
DATA:bfs_c++_thread,4,16000000,2.677000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.645000
DATA:bfs_c++_async,4,16000000,2.645000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 2.004000
DATA:bfs_omp_for,8,16000000,2.004000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.561000
DATA:bfs_omp_task,8,16000000,1.561000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.614000
DATA:bfs_cilk_for,8,16000000,1.614000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.691000
DATA:bfs_cilk_spawn,8,16000000,1.691000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.657000
DATA:bfs_c++_thread,8,16000000,1.657000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.583000
DATA:bfs_c++_async,8,16000000,1.583000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 1.657000
DATA:bfs_omp_for,16,16000000,1.657000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.173000
DATA:bfs_omp_task,16,16000000,1.173000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.233000
DATA:bfs_cilk_for,16,16000000,1.233000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.367000
DATA:bfs_cilk_spawn,16,16000000,1.367000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.186000
DATA:bfs_c++_thread,16,16000000,1.186000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.195000
DATA:bfs_c++_async,16,16000000,1.195000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 0.922000
DATA:bfs_omp_for,32,16000000,0.922000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.907000
DATA:bfs_omp_task,32,16000000,0.907000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.985000
DATA:bfs_cilk_for,32,16000000,0.985000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.094000
DATA:bfs_cilk_spawn,32,16000000,1.094000,271537890
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.906000
DATA:bfs_c++_thread,32,16000000,0.906000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.921000
DATA:bfs_c++_async,32,16000000,0.921000,271471647
Result stored in result_bfs_c++_async.txt
