THREADS 1
Reading File
Start traversing the tree
Compute time: 8.393000
DATA:bfs_omp_for,1,16000000,8.393000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 8.413000
DATA:bfs_omp_task,1,16000000,8.413000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 8.986000
DATA:bfs_cilk_for,1,16000000,8.986000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 8.423000
DATA:bfs_cilk_spawn,1,16000000,8.423000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 8.861000
DATA:bfs_c++_thread,1,16000000,8.861000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 8.514000
DATA:bfs_c++_async,1,16000000,8.514000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 4.269000
DATA:bfs_omp_for,2,16000000,4.269000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 4.115000
DATA:bfs_omp_task,2,16000000,4.115000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 4.273000
DATA:bfs_cilk_for,2,16000000,4.273000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 4.400000
DATA:bfs_cilk_spawn,2,16000000,4.400000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 5.064000
DATA:bfs_c++_thread,2,16000000,5.064000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 4.896000
DATA:bfs_c++_async,2,16000000,4.896000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 2.688000
DATA:bfs_omp_for,4,16000000,2.688000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 2.587000
DATA:bfs_omp_task,4,16000000,2.587000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.944000
DATA:bfs_cilk_for,4,16000000,2.944000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 2.662000
DATA:bfs_cilk_spawn,4,16000000,2.662000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.694000
DATA:bfs_c++_thread,4,16000000,2.694000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.790000
DATA:bfs_c++_async,4,16000000,2.790000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 2.081000
DATA:bfs_omp_for,8,16000000,2.081000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.680000
DATA:bfs_omp_task,8,16000000,1.680000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.642000
DATA:bfs_cilk_for,8,16000000,1.642000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.632000
DATA:bfs_cilk_spawn,8,16000000,1.632000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.778000
DATA:bfs_c++_thread,8,16000000,1.778000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.693000
DATA:bfs_c++_async,8,16000000,1.693000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 1.127000
DATA:bfs_omp_for,16,16000000,1.127000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.200000
DATA:bfs_omp_task,16,16000000,1.200000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.192000
DATA:bfs_cilk_for,16,16000000,1.192000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.460000
DATA:bfs_cilk_spawn,16,16000000,1.460000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.158000
DATA:bfs_c++_thread,16,16000000,1.158000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.154000
DATA:bfs_c++_async,16,16000000,1.154000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 0.984000
DATA:bfs_omp_for,32,16000000,0.984000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.880000
DATA:bfs_omp_task,32,16000000,0.880000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.961000
DATA:bfs_cilk_for,32,16000000,0.961000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.112000
DATA:bfs_cilk_spawn,32,16000000,1.112000,271751524
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.948000
DATA:bfs_c++_thread,32,16000000,0.948000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.990000
DATA:bfs_c++_async,32,16000000,0.990000,271471647
Result stored in result_bfs_c++_async.txt
