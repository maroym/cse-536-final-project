THREADS 1
Reading File
Start traversing the tree
Compute time: 8.613000
DATA:bfs_omp_for,1,16000000,8.613000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 8.157000
DATA:bfs_omp_task,1,16000000,8.157000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 9.069000
DATA:bfs_cilk_for,1,16000000,9.069000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 8.550000
DATA:bfs_cilk_spawn,1,16000000,8.550000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 8.654000
DATA:bfs_c++_thread,1,16000000,8.654000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 8.841000
DATA:bfs_c++_async,1,16000000,8.841000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 4.228000
DATA:bfs_omp_for,2,16000000,4.228000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 4.274000
DATA:bfs_omp_task,2,16000000,4.274000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 4.274000
DATA:bfs_cilk_for,2,16000000,4.274000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 4.683000
DATA:bfs_cilk_spawn,2,16000000,4.683000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 5.043000
DATA:bfs_c++_thread,2,16000000,5.043000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 4.819000
DATA:bfs_c++_async,2,16000000,4.819000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 2.592000
DATA:bfs_omp_for,4,16000000,2.592000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 2.556000
DATA:bfs_omp_task,4,16000000,2.556000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.548000
DATA:bfs_cilk_for,4,16000000,2.548000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 2.873000
DATA:bfs_cilk_spawn,4,16000000,2.873000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.647000
DATA:bfs_c++_thread,4,16000000,2.647000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.695000
DATA:bfs_c++_async,4,16000000,2.695000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 1.606000
DATA:bfs_omp_for,8,16000000,1.606000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.559000
DATA:bfs_omp_task,8,16000000,1.559000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.585000
DATA:bfs_cilk_for,8,16000000,1.585000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.726000
DATA:bfs_cilk_spawn,8,16000000,1.726000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.661000
DATA:bfs_c++_thread,8,16000000,1.661000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.646000
DATA:bfs_c++_async,8,16000000,1.646000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 1.191000
DATA:bfs_omp_for,16,16000000,1.191000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.142000
DATA:bfs_omp_task,16,16000000,1.142000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.176000
DATA:bfs_cilk_for,16,16000000,1.176000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.163000
DATA:bfs_cilk_spawn,16,16000000,1.163000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.156000
DATA:bfs_c++_thread,16,16000000,1.156000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.156000
DATA:bfs_c++_async,16,16000000,1.156000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 1.427000
DATA:bfs_omp_for,32,16000000,1.427000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 0.887000
DATA:bfs_omp_task,32,16000000,0.887000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.010000
DATA:bfs_cilk_for,32,16000000,1.010000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.097000
DATA:bfs_cilk_spawn,32,16000000,1.097000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.904000
DATA:bfs_c++_thread,32,16000000,0.904000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.927000
DATA:bfs_c++_async,32,16000000,0.927000,271471647
Result stored in result_bfs_c++_async.txt
