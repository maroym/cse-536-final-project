THREADS 1
Reading File
Start traversing the tree
Compute time: 1.291000
DATA:bfs_omp_for,1,16000000,1.291000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 6.291000
DATA:bfs_omp_task,1,16000000,6.291000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.721000
DATA:bfs_cilk_for,1,16000000,1.721000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 7.485000
DATA:bfs_cilk_spawn,1,16000000,7.485000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 6.993000
DATA:bfs_c++_thread,1,16000000,6.993000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 6.976000
DATA:bfs_c++_async,1,16000000,6.976000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 1.906000
DATA:bfs_omp_for,2,16000000,1.906000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 3.675000
DATA:bfs_omp_task,2,16000000,3.675000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.118000
DATA:bfs_cilk_for,2,16000000,1.118000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 5.213000
DATA:bfs_cilk_spawn,2,16000000,5.213000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 4.186000
DATA:bfs_c++_thread,2,16000000,4.186000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 4.764000
DATA:bfs_c++_async,2,16000000,4.764000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 1.342000
DATA:bfs_omp_for,4,16000000,1.342000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 3.055000
DATA:bfs_omp_task,4,16000000,3.055000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.708000
DATA:bfs_cilk_for,4,16000000,1.708000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 2.800000
DATA:bfs_cilk_spawn,4,16000000,2.800000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.779000
DATA:bfs_c++_thread,4,16000000,2.779000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.775000
DATA:bfs_c++_async,4,16000000,2.775000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 1.268000
DATA:bfs_omp_for,8,16000000,1.268000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.720000
DATA:bfs_omp_task,8,16000000,1.720000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.135000
DATA:bfs_cilk_for,8,16000000,1.135000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.983000
DATA:bfs_cilk_spawn,8,16000000,1.983000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.727000
DATA:bfs_c++_thread,8,16000000,1.727000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.776000
DATA:bfs_c++_async,8,16000000,1.776000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 1.878000
DATA:bfs_omp_for,16,16000000,1.878000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.580000
DATA:bfs_omp_task,16,16000000,1.580000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.708000
DATA:bfs_cilk_for,16,16000000,1.708000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.314000
DATA:bfs_cilk_spawn,16,16000000,1.314000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.243000
DATA:bfs_c++_thread,16,16000000,1.243000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.218000
DATA:bfs_c++_async,16,16000000,1.218000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 1.318000
DATA:bfs_omp_for,32,16000000,1.318000,271471647
Result stored in result_bfs_omp_for.txt
Reading File
Start traversing the tree
Compute time: 1.420000
DATA:bfs_omp_task,32,16000000,1.420000,271471647
Result stored in result_bfs_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.159000
DATA:bfs_cilk_for,32,16000000,1.159000,271471647
Result stored in result_bfs_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.146000
DATA:bfs_cilk_spawn,32,16000000,1.146000,271471647
Result stored in result_bfs_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.972000
DATA:bfs_c++_thread,32,16000000,0.972000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.975000
DATA:bfs_c++_async,32,16000000,0.975000,271471647
Result stored in result_bfs_c++_async.txt
