THREADS 1
Reading File
Start traversing the tree
Compute time: 0.961013
DATA:bfs_omp_parallel,1,4000000,0.961013,63640867
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 1.036826
DATA:bfs_omp_task,1,4000000,1.036826,63640867
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.892000
DATA:bfs_cilkplus_cilk_for,1,4000000,0.892000,63640867
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.004000
DATA:bfs_cilkplus_cilk_spawn,1,4000000,1.004000,63640867
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.151000
DATA:bfs_c++_thread,1,4000000,1.151000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.875000
DATA:bfs_c++_async,1,4000000,0.875000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 0.754503
DATA:bfs_omp_parallel,2,4000000,0.754503,63640867
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.955905
DATA:bfs_omp_task,2,4000000,0.955905,63640867
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.687000
DATA:bfs_cilkplus_cilk_for,2,4000000,0.687000,63640867
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.740000
DATA:bfs_cilkplus_cilk_spawn,2,4000000,0.740000,63640867
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.611000
DATA:bfs_c++_thread,2,4000000,0.611000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.737000
DATA:bfs_c++_async,2,4000000,0.737000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 0.507592
DATA:bfs_omp_parallel,4,4000000,0.507592,63640867
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.484078
DATA:bfs_omp_task,4,4000000,0.484078,63640867
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.329000
DATA:bfs_cilkplus_cilk_for,4,4000000,0.329000,63640867
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.509000
DATA:bfs_cilkplus_cilk_spawn,4,4000000,0.509000,63640867
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.332000
DATA:bfs_c++_thread,4,4000000,0.332000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.340000
DATA:bfs_c++_async,4,4000000,0.340000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 0.530828
DATA:bfs_omp_parallel,8,4000000,0.530828,63640867
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.498980
DATA:bfs_omp_task,8,4000000,0.498980,63640867
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.265000
DATA:bfs_cilkplus_cilk_for,8,4000000,0.265000,63640867
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.403000
DATA:bfs_cilkplus_cilk_spawn,8,4000000,0.403000,63640867
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.225000
DATA:bfs_c++_thread,8,4000000,0.225000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.225000
DATA:bfs_c++_async,8,4000000,0.225000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 0.297722
DATA:bfs_omp_parallel,16,4000000,0.297722,63640867
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.166341
DATA:bfs_omp_task,16,4000000,0.166341,63640867
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.296000
DATA:bfs_cilkplus_cilk_for,16,4000000,0.296000,63640867
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.156000
DATA:bfs_cilkplus_cilk_spawn,16,4000000,0.156000,63640867
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.162000
DATA:bfs_c++_thread,16,4000000,0.162000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.160000
DATA:bfs_c++_async,16,4000000,0.160000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 0.273091
DATA:bfs_omp_parallel,32,4000000,0.273091,63640867
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.189967
DATA:bfs_omp_task,32,4000000,0.189967,63640867
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.287000
DATA:bfs_cilkplus_cilk_for,32,4000000,0.287000,63640867
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.228000
DATA:bfs_cilkplus_cilk_spawn,32,4000000,0.228000,63640867
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.131000
DATA:bfs_c++_thread,32,4000000,0.131000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.128000
DATA:bfs_c++_async,32,4000000,0.128000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 1
Reading File
Start traversing the tree
Compute time: 2.705557
DATA:bfs_omp_parallel,1,8000000,2.705557,132105797
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 2.342366
DATA:bfs_omp_task,1,8000000,2.342366,132105797
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.012000
DATA:bfs_cilkplus_cilk_for,1,8000000,2.012000,132105797
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 2.366000
DATA:bfs_cilkplus_cilk_spawn,1,8000000,2.366000,132105797
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.635000
DATA:bfs_c++_thread,1,8000000,2.635000,132105797
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.442000
DATA:bfs_c++_async,1,8000000,2.442000,132105797
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 1.636511
DATA:bfs_omp_parallel,2,8000000,1.636511,132105797
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 1.350788
DATA:bfs_omp_task,2,8000000,1.350788,132105797
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.501000
DATA:bfs_cilkplus_cilk_for,2,8000000,1.501000,132105797
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.555000
DATA:bfs_cilkplus_cilk_spawn,2,8000000,1.555000,132105797
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.324000
DATA:bfs_c++_thread,2,8000000,1.324000,132105797
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.336000
DATA:bfs_c++_async,2,8000000,1.336000,132105797
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 1.051666
DATA:bfs_omp_parallel,4,8000000,1.051666,132105797
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 1.159884
DATA:bfs_omp_task,4,8000000,1.159884,132105797
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.965000
DATA:bfs_cilkplus_cilk_for,4,8000000,0.965000,132105797
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.923000
DATA:bfs_cilkplus_cilk_spawn,4,8000000,0.923000,132105797
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.714000
DATA:bfs_c++_thread,4,8000000,0.714000,132105797
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.709000
DATA:bfs_c++_async,4,8000000,0.709000,132105797
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 1.025156
DATA:bfs_omp_parallel,8,8000000,1.025156,132105797
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.826279
DATA:bfs_omp_task,8,8000000,0.826279,132105797
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.731000
DATA:bfs_cilkplus_cilk_for,8,8000000,0.731000,132105797
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.619000
DATA:bfs_cilkplus_cilk_spawn,8,8000000,0.619000,132105797
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.470000
DATA:bfs_c++_thread,8,8000000,0.470000,132105797
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.483000
DATA:bfs_c++_async,8,8000000,0.483000,132105797
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 0.626779
DATA:bfs_omp_parallel,16,8000000,0.626779,132105797
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.580058
DATA:bfs_omp_task,16,8000000,0.580058,132105797
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.608000
DATA:bfs_cilkplus_cilk_for,16,8000000,0.608000,132105797
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.372000
DATA:bfs_cilkplus_cilk_spawn,16,8000000,0.372000,132105797
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.329000
DATA:bfs_c++_thread,16,8000000,0.329000,132105797
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.332000
DATA:bfs_c++_async,16,8000000,0.332000,132105797
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 0.539288
DATA:bfs_omp_parallel,32,8000000,0.539288,132105797
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.581749
DATA:bfs_omp_task,32,8000000,0.581749,132105797
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.559000
DATA:bfs_cilkplus_cilk_for,32,8000000,0.559000,132105797
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.364000
DATA:bfs_cilkplus_cilk_spawn,32,8000000,0.364000,132105797
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.253000
DATA:bfs_c++_thread,32,8000000,0.253000,132105797
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.248000
DATA:bfs_c++_async,32,8000000,0.248000,132105797
Result stored in result_bfs_c++_async.txt
THREADS 1
Reading File
Start traversing the tree
Compute time: 6.214824
DATA:bfs_omp_parallel,1,16000000,6.214824,271471647
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 6.504541
DATA:bfs_omp_task,1,16000000,6.504541,271471647
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 5.136000
DATA:bfs_cilkplus_cilk_for,1,16000000,5.136000,271471647
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 4.796000
DATA:bfs_cilkplus_cilk_spawn,1,16000000,4.796000,271471647
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 4.907000
DATA:bfs_c++_thread,1,16000000,4.907000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 5.504000
DATA:bfs_c++_async,1,16000000,5.504000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 3.281021
DATA:bfs_omp_parallel,2,16000000,3.281021,271471647
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 3.368155
DATA:bfs_omp_task,2,16000000,3.368155,271471647
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.906000
DATA:bfs_cilkplus_cilk_for,2,16000000,2.906000,271471647
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 3.480000
DATA:bfs_cilkplus_cilk_spawn,2,16000000,3.480000,271471647
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.912000
DATA:bfs_c++_thread,2,16000000,2.912000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.954000
DATA:bfs_c++_async,2,16000000,2.954000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 2.093624
DATA:bfs_omp_parallel,4,16000000,2.093624,271471647
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 2.448599
DATA:bfs_omp_task,4,16000000,2.448599,271471647
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.183000
DATA:bfs_cilkplus_cilk_for,4,16000000,2.183000,271471647
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.917000
DATA:bfs_cilkplus_cilk_spawn,4,16000000,1.917000,271471647
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.654000
DATA:bfs_c++_thread,4,16000000,1.654000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.616000
DATA:bfs_c++_async,4,16000000,1.616000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 1.258280
DATA:bfs_omp_parallel,8,16000000,1.258280,271471647
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 1.228251
DATA:bfs_omp_task,8,16000000,1.228251,271471647
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.646000
DATA:bfs_cilkplus_cilk_for,8,16000000,1.646000,271471647
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.149000
DATA:bfs_cilkplus_cilk_spawn,8,16000000,1.149000,271471647
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.998000
DATA:bfs_c++_thread,8,16000000,0.998000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.020000
DATA:bfs_c++_async,8,16000000,1.020000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 1.282938
DATA:bfs_omp_parallel,16,16000000,1.282938,271471647
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.931653
DATA:bfs_omp_task,16,16000000,0.931653,271471647
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.271000
DATA:bfs_cilkplus_cilk_for,16,16000000,1.271000,271471647
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.699000
DATA:bfs_cilkplus_cilk_spawn,16,16000000,0.699000,271471647
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.660000
DATA:bfs_c++_thread,16,16000000,0.660000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.691000
DATA:bfs_c++_async,16,16000000,0.691000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 1.122273
DATA:bfs_omp_parallel,32,16000000,1.122273,271471647
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.974484
DATA:bfs_omp_task,32,16000000,0.974484,271471647
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.155000
DATA:bfs_cilkplus_cilk_for,32,16000000,1.155000,271471647
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.696000
DATA:bfs_cilkplus_cilk_spawn,32,16000000,0.696000,271471647
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.523000
DATA:bfs_c++_thread,32,16000000,0.523000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.525000
DATA:bfs_c++_async,32,16000000,0.525000,271471647
Result stored in result_bfs_c++_async.txt
