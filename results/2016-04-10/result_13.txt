THREADS 1
Reading File
Start traversing the tree
Compute time: 0.914626
DATA:bfs_omp_parallel,1,4000000,0.914626,63640867
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.982060
DATA:bfs_omp_task,1,4000000,0.982060,63640867
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.860000
DATA:bfs_cilkplus_cilk_for,1,4000000,0.860000,63640867
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.918000
DATA:bfs_cilkplus_cilk_spawn,1,4000000,0.918000,63640867
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.018000
DATA:bfs_c++_thread,1,4000000,1.018000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.194000
DATA:bfs_c++_async,1,4000000,1.194000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 0.677517
DATA:bfs_omp_parallel,2,4000000,0.677517,63640867
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.622158
DATA:bfs_omp_task,2,4000000,0.622158,63640867
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.447000
DATA:bfs_cilkplus_cilk_for,2,4000000,0.447000,63640867
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.683000
DATA:bfs_cilkplus_cilk_spawn,2,4000000,0.683000,63640867
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.484000
DATA:bfs_c++_thread,2,4000000,0.484000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.616000
DATA:bfs_c++_async,2,4000000,0.616000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 0.499801
DATA:bfs_omp_parallel,4,4000000,0.499801,63640867
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.493253
DATA:bfs_omp_task,4,4000000,0.493253,63640867
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.458000
DATA:bfs_cilkplus_cilk_for,4,4000000,0.458000,63640867
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.454000
DATA:bfs_cilkplus_cilk_spawn,4,4000000,0.454000,63640867
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.353000
DATA:bfs_c++_thread,4,4000000,0.353000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.367000
DATA:bfs_c++_async,4,4000000,0.367000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 0.517498
DATA:bfs_omp_parallel,8,4000000,0.517498,63640867
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.411136
DATA:bfs_omp_task,8,4000000,0.411136,63640867
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.471000
DATA:bfs_cilkplus_cilk_for,8,4000000,0.471000,63640867
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.264000
DATA:bfs_cilkplus_cilk_spawn,8,4000000,0.264000,63640867
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.222000
DATA:bfs_c++_thread,8,4000000,0.222000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.243000
DATA:bfs_c++_async,8,4000000,0.243000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 0.161351
DATA:bfs_omp_parallel,16,4000000,0.161351,63640867
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.329312
DATA:bfs_omp_task,16,4000000,0.329312,63640867
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.312000
DATA:bfs_cilkplus_cilk_for,16,4000000,0.312000,63640867
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.216000
DATA:bfs_cilkplus_cilk_spawn,16,4000000,0.216000,63640867
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.161000
DATA:bfs_c++_thread,16,4000000,0.161000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.164000
DATA:bfs_c++_async,16,4000000,0.164000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 0.263069
DATA:bfs_omp_parallel,32,4000000,0.263069,63640867
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.132049
DATA:bfs_omp_task,32,4000000,0.132049,63640867
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.286000
DATA:bfs_cilkplus_cilk_for,32,4000000,0.286000,63640867
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.153000
DATA:bfs_cilkplus_cilk_spawn,32,4000000,0.153000,63640867
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.163000
DATA:bfs_c++_thread,32,4000000,0.163000,63640867
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.135000
DATA:bfs_c++_async,32,4000000,0.135000,63640867
Result stored in result_bfs_c++_async.txt
THREADS 1
Reading File
Start traversing the tree
Compute time: 2.291870
DATA:bfs_omp_parallel,1,8000000,2.291870,132105797
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 2.380490
DATA:bfs_omp_task,1,8000000,2.380490,132105797
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 2.051000
DATA:bfs_cilkplus_cilk_for,1,8000000,2.051000,132105797
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 2.462000
DATA:bfs_cilkplus_cilk_spawn,1,8000000,2.462000,132105797
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 2.515000
DATA:bfs_c++_thread,1,8000000,2.515000,132105797
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 2.098000
DATA:bfs_c++_async,1,8000000,2.098000,132105797
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 1.366424
DATA:bfs_omp_parallel,2,8000000,1.366424,132105797
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 1.710109
DATA:bfs_omp_task,2,8000000,1.710109,132105797
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.213000
DATA:bfs_cilkplus_cilk_for,2,8000000,1.213000,132105797
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.508000
DATA:bfs_cilkplus_cilk_spawn,2,8000000,1.508000,132105797
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.352000
DATA:bfs_c++_thread,2,8000000,1.352000,132105797
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.296000
DATA:bfs_c++_async,2,8000000,1.296000,132105797
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 1.007301
DATA:bfs_omp_parallel,4,8000000,1.007301,132105797
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.949628
DATA:bfs_omp_task,4,8000000,0.949628,132105797
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.970000
DATA:bfs_cilkplus_cilk_for,4,8000000,0.970000,132105797
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.876000
DATA:bfs_cilkplus_cilk_spawn,4,8000000,0.876000,132105797
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.745000
DATA:bfs_c++_thread,4,8000000,0.745000,132105797
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.726000
DATA:bfs_c++_async,4,8000000,0.726000,132105797
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 0.480425
DATA:bfs_omp_parallel,8,8000000,0.480425,132105797
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.622998
DATA:bfs_omp_task,8,8000000,0.622998,132105797
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.732000
DATA:bfs_cilkplus_cilk_for,8,8000000,0.732000,132105797
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.507000
DATA:bfs_cilkplus_cilk_spawn,8,8000000,0.507000,132105797
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.474000
DATA:bfs_c++_thread,8,8000000,0.474000,132105797
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.470000
DATA:bfs_c++_async,8,8000000,0.470000,132105797
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 0.318054
DATA:bfs_omp_parallel,16,8000000,0.318054,132105797
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.652295
DATA:bfs_omp_task,16,8000000,0.652295,132105797
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.617000
DATA:bfs_cilkplus_cilk_for,16,8000000,0.617000,132105797
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.418000
DATA:bfs_cilkplus_cilk_spawn,16,8000000,0.418000,132105797
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.337000
DATA:bfs_c++_thread,16,8000000,0.337000,132105797
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.323000
DATA:bfs_c++_async,16,8000000,0.323000,132105797
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 0.246792
DATA:bfs_omp_parallel,32,8000000,0.246792,132105797
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 0.693984
DATA:bfs_omp_task,32,8000000,0.693984,132105797
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 0.376000
DATA:bfs_cilkplus_cilk_for,32,8000000,0.376000,132105797
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.324000
DATA:bfs_cilkplus_cilk_spawn,32,8000000,0.324000,132105797
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.256000
DATA:bfs_c++_thread,32,8000000,0.256000,132105797
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.265000
DATA:bfs_c++_async,32,8000000,0.265000,132105797
Result stored in result_bfs_c++_async.txt
THREADS 1
Reading File
Start traversing the tree
Compute time: 5.640090
DATA:bfs_omp_parallel,1,16000000,5.640090,271471647
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 5.869916
DATA:bfs_omp_task,1,16000000,5.869916,271471647
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 5.242000
DATA:bfs_cilkplus_cilk_for,1,16000000,5.242000,271471647
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 5.912000
DATA:bfs_cilkplus_cilk_spawn,1,16000000,5.912000,271471647
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 5.647000
DATA:bfs_c++_thread,1,16000000,5.647000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 6.145000
DATA:bfs_c++_async,1,16000000,6.145000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 2
Reading File
Start traversing the tree
Compute time: 3.503028
DATA:bfs_omp_parallel,2,16000000,3.503028,271471647
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 3.848142
DATA:bfs_omp_task,2,16000000,3.848142,271471647
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 3.380000
DATA:bfs_cilkplus_cilk_for,2,16000000,3.380000,271471647
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 3.427000
DATA:bfs_cilkplus_cilk_spawn,2,16000000,3.427000,271471647
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 3.118000
DATA:bfs_c++_thread,2,16000000,3.118000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 3.158000
DATA:bfs_c++_async,2,16000000,3.158000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 4
Reading File
Start traversing the tree
Compute time: 2.295810
DATA:bfs_omp_parallel,4,16000000,2.295810,271471647
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 2.699497
DATA:bfs_omp_task,4,16000000,2.699497,271471647
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.780000
DATA:bfs_cilkplus_cilk_for,4,16000000,1.780000,271471647
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.877000
DATA:bfs_cilkplus_cilk_spawn,4,16000000,1.877000,271471647
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.749000
DATA:bfs_c++_thread,4,16000000,1.749000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.600000
DATA:bfs_c++_async,4,16000000,1.600000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 8
Reading File
Start traversing the tree
Compute time: 1.745206
DATA:bfs_omp_parallel,8,16000000,1.745206,271471647
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 2.046743
DATA:bfs_omp_task,8,16000000,2.046743,271471647
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.515000
DATA:bfs_cilkplus_cilk_for,8,16000000,1.515000,271471647
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 1.106000
DATA:bfs_cilkplus_cilk_spawn,8,16000000,1.106000,271471647
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 1.073000
DATA:bfs_c++_thread,8,16000000,1.073000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 1.065000
DATA:bfs_c++_async,8,16000000,1.065000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 16
Reading File
Start traversing the tree
Compute time: 0.961242
DATA:bfs_omp_parallel,16,16000000,0.961242,271471647
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 1.465269
DATA:bfs_omp_task,16,16000000,1.465269,271471647
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.238000
DATA:bfs_cilkplus_cilk_for,16,16000000,1.238000,271471647
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.850000
DATA:bfs_cilkplus_cilk_spawn,16,16000000,0.850000,271471647
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.716000
DATA:bfs_c++_thread,16,16000000,0.716000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.707000
DATA:bfs_c++_async,16,16000000,0.707000,271471647
Result stored in result_bfs_c++_async.txt
THREADS 32
Reading File
Start traversing the tree
Compute time: 1.758568
DATA:bfs_omp_parallel,32,16000000,1.758568,271471647
Result stored in result_omp_parallel.txt
Reading File
Start traversing the tree
Compute time: 1.195019
DATA:bfs_omp_task,32,16000000,1.195019,271471647
Result stored in result_omp_task.txt
Reading File
Start traversing the tree
Compute time: 1.158000
DATA:bfs_cilkplus_cilk_for,32,16000000,1.158000,271471647
Result stored in result_cilkplus_cilk_for.txt
Reading File
Start traversing the tree
Compute time: 0.607000
DATA:bfs_cilkplus_cilk_spawn,32,16000000,0.607000,271527037
Result stored in result_cilkplus_cilk_spawn.txt
Reading File
Start traversing the tree
Compute time: 0.562000
DATA:bfs_c++_thread,32,16000000,0.562000,271471647
Result stored in result_bfs_c++_thread.txt
Reading File
Start traversing the tree
Compute time: 0.527000
DATA:bfs_c++_async,32,16000000,0.527000,271471647
Result stored in result_bfs_c++_async.txt
