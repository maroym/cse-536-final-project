import sys

def avg(l):
	return sum(l) / float(len(l))

data = dict()
with open(sys.argv[1]) as f:
	for line in f.readlines():
		parts = line.strip().split(',')
		key = ','.join(parts[0:3])
		if not key in data:
			data[key] = []
		
		data[key].append(float(parts[3]))
	
	for key in data:
		print key + ',' + str(avg(data[key]))

		