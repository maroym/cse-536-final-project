#include <stdio.h>
#include <sys/timeb.h>

double read_timer() {
    struct timeb tm;
    ftime(&tm);
    return (double) tm.time + (double) tm.millitm / 1000.0;
}

int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        printf("usage error");
        return -1;
    }

    int value, result=0, count=0;
    FILE* fpText, *fpBinary;

    double start, end;

    start = read_timer();
    count = 0;
    fpText = fopen(argv[1],"r");
    while(!feof(fpText))
    {
        result = fscanf(fpText,"%d",&value);
        if (1 == result)
        {
            count++;
        }
    }
    fclose(fpText);
    end = read_timer();
    printf("Count: %d, %f\n", count, end - start);

    start = read_timer();
    count = 0;
    fpBinary = fopen(argv[2],"r");
    while(!feof(fpBinary))
    {
        result = fread(&value, sizeof(int), 1, fpBinary);
        if (1 == result)
        {
            count++;
        }
    }
    fclose(fpBinary);
    end = read_timer();
    printf("Count: %d, %f\n", count, end - start);

    return 0;    
}