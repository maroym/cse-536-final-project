#include "shared.h"

void phase_one(int start, int end, bool *h_graph_mask, Node *h_graph_nodes, bool *h_graph_visited, int *h_cost, bool *h_updating_graph_mask, int *h_graph_edges)
{
    for(int n = start; n < end; n++ )
    {
        phase_one_single(n, h_graph_mask, h_graph_nodes, h_graph_visited, h_cost, h_updating_graph_mask, h_graph_edges);
    }
}

void phase_two(int start, int end, bool *keep_going, bool *h_graph_mask, bool *h_graph_visited, bool *h_updating_graph_mask)
{
    for(int n=start; n<end; n++ )
    {
        phase_two_single(n, h_updating_graph_mask, h_graph_mask, h_graph_visited, keep_going);
    }
}

void dist(int tid, int num_tasks, int num_nodes, int *start, int *end )
{
   
    int Nt;
    int remain = num_nodes % num_tasks;
    int esize = num_nodes / num_tasks;

    if (tid < remain)
    {
        /* each of the first remain task has one more element */
        Nt = esize + 1;
        *start = Nt * tid;
    } else {
        Nt = esize;
        *start = esize * tid + remain;
    }
    *end = *start + Nt;
}