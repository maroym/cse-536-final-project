#ifndef __BFS_SHARED_H__
#define __BFS_SHARED_H__

//Structure to hold node information
struct Node
{
    int starting;
    int no_of_edges;
};

// each framework/feature sets its own algorithm_name
extern const char* algorithm_name;

// actual implementation in framework/feature specific cpp file
void run(int num_threads, int no_of_nodes, bool* h_graph_mask, Node* h_graph_nodes, bool* h_graph_visited, int* h_cost, bool* h_updating_graph_mask, int *h_graph_edges);

void phase_one(int start, int end, bool *h_graph_mask, Node *h_graph_nodes, bool *h_graph_visited, int *h_cost, bool *h_updating_graph_mask, int *h_graph_edges);
void phase_two(int start, int end, bool *keep_going, bool *h_graph_mask, bool *h_graph_visited, bool *h_updating_graph_mask);

void dist(int tid, int num_tasks, int num_nodes, int *start, int *end);

// called in a tight loop, inline to avoid unecessary stack churn
inline void phase_one_single(int tid, bool *h_graph_mask, Node* h_graph_nodes, bool *h_graph_visited, int* h_cost, bool *h_updating_graph_mask, int *h_graph_edges)
{
    if (h_graph_mask[tid] == true)
    {
        h_graph_mask[tid]=false;
        for(int i=h_graph_nodes[tid].starting; i<(h_graph_nodes[tid].no_of_edges + h_graph_nodes[tid].starting); i++)
        {
            int id = h_graph_edges[i];
            if(!h_graph_visited[id])
            {
                h_cost[id]=h_cost[tid]+1;
                h_updating_graph_mask[id]=true;
            }
        }
    }
}

// called in a tight loop, inline to avoid unecessary stack churn
inline void phase_two_single(int tid, bool *h_updating_graph_mask, bool *h_graph_mask, bool* h_graph_visited, bool *keep_going)
{
    if (h_updating_graph_mask[tid] == true)
    {
        h_graph_mask[tid]=true;
        h_graph_visited[tid]=true;
        *keep_going=true;
        h_updating_graph_mask[tid]=false;
   };
}

#endif //__BFS_SHARED_H__