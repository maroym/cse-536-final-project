#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <sys/timeb.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>

#include "shared.h"
const char *algorithm_name = "bfs_cilk_for";

void run(int num_threads, int no_of_nodes, bool* h_graph_mask, Node* h_graph_nodes, bool* h_graph_visited, int* h_cost, bool* h_updating_graph_mask, int *h_graph_edges)
{
    char cilk_nworkers[3];
    sprintf(cilk_nworkers, "%d", num_threads);
    __cilkrts_set_param("nworkers", cilk_nworkers);

    bool keep_going;
    do
    {
        //if no thread changes this value then the loop stops
        keep_going=false;
        int tid;
        cilk_for(int tid = 0; tid < no_of_nodes; tid++ )
        {
            phase_one_single(tid, h_graph_mask, h_graph_nodes, h_graph_visited, h_cost, h_updating_graph_mask, h_graph_edges);
        }


        cilk_for(int tid=0; tid< no_of_nodes ; tid++ )
        {
            phase_two_single(tid, h_updating_graph_mask, h_graph_mask, h_graph_visited, &keep_going);
        }
    }
    while(keep_going);
}
