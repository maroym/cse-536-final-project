#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <sys/timeb.h>

#include "shared.h"

using namespace std;

/* read timer in second */
double read_timer() {
    struct timeb tm;
    ftime(&tm);
    return (double) tm.time + (double) tm.millitm / 1000.0;
}

void bfs_graph(int num_threads, char* input_f);

void usage_message(int argc, char**argv)
{
    fprintf(stderr,"Usage: %s <num_threads> <input_file>\n", argv[0]);
}

////////////////////////////////////////////////////////////////////////////////
// Main Program
////////////////////////////////////////////////////////////////////////////////
int main( int argc, char** argv)
{
    if(argc!=3)
    {
        usage_message(argc, argv);
        exit(0);
    }

    int num_threads = atoi(argv[1]);
    char* input_f = argv[2];

    bfs_graph(num_threads, input_f);
}

////////////////////////////////////////////////////////////////////////////////
// Apply BFS on a Graph
////////////////////////////////////////////////////////////////////////////////
void bfs_graph(int num_threads, char* input_f)
{
    int no_of_nodes = 0;
    int edge_list_size = 0;
    int source = 0;

    printf("Reading File\n");
    FILE *fp = fopen(input_f,"r");
    if(!fp)
    {
        printf("Error Reading graph file\n");
        return;
    }

    fread(&no_of_nodes,sizeof(int),1,fp);

    // allocate host memory
    Node* h_graph_nodes = (Node*) malloc(sizeof(Node)*no_of_nodes);
    bool *h_graph_mask = (bool*) malloc(sizeof(bool)*no_of_nodes);
    bool *h_updating_graph_mask = (bool*) malloc(sizeof(bool)*no_of_nodes);
    bool *h_graph_visited = (bool*) malloc(sizeof(bool)*no_of_nodes);

    int start, edgeno;
    // initalize the memory
    for( unsigned int i = 0; i < no_of_nodes; i++)
    {
        fread(&start,sizeof(int),1,fp);
        fread(&edgeno,sizeof(int),1,fp);
        h_graph_nodes[i].starting = start;
        h_graph_nodes[i].no_of_edges = edgeno;
        h_graph_mask[i]=false;
        h_updating_graph_mask[i]=false;
        h_graph_visited[i]=false;
    }

    //read the source node from the file
    fread(&source,sizeof(int),1,fp);

    //set the source node as true in the mask
    h_graph_mask[source]=true;
    h_graph_visited[source]=true;

    fread(&edge_list_size,sizeof(int),1,fp);

    int id,cost;
    int* h_graph_edges = (int*) malloc(sizeof(int)*edge_list_size);
    for(int i=0; i < edge_list_size ; i++)
    {
        fread(&id,sizeof(int),1,fp);
        fread(&cost,sizeof(int),1,fp);
        h_graph_edges[i] = id;
    }

    fclose(fp);

    // allocate mem for the result
    int* h_cost = (int*) malloc( sizeof(int)*no_of_nodes);
    for(int i=0;i<no_of_nodes;i++)
    {
        h_cost[i]=-1;
    }
    h_cost[source]=0;

    printf("Start traversing the tree\n");

    double start_time = read_timer();

    // The run function for each framework/feature is defined in a separate .cpp
    // and compiled and linked by the Makefile
    run(num_threads, no_of_nodes, h_graph_mask, h_graph_nodes, h_graph_visited, h_cost, h_updating_graph_mask, h_graph_edges);

    double end_time = read_timer();
    printf("Compute time: %lf\n", (end_time - start_time));

    // Store the result into a file
    char result_file_name[50];
    sprintf(result_file_name, "result_%s.txt", algorithm_name);
    FILE *fpo = fopen(result_file_name,"w");
    for(int i=0;i<no_of_nodes;i++)
    {
        fprintf(fpo,"%d) cost:%d\n",i,h_cost[i]);
    }
    
    fseek(fpo, 0L, SEEK_END);
    printf("DATA:%s,%d,%d,%lf,%ld\n",algorithm_name, num_threads,no_of_nodes,(end_time - start_time),ftell(fpo));

    fclose(fpo);
    printf("Result stored in %s\n", result_file_name);

    // cleanup memory
    free(h_graph_nodes);
    free(h_graph_edges);
    free(h_graph_mask);
    free(h_updating_graph_mask);
    free(h_graph_visited);
    free(h_cost);
}