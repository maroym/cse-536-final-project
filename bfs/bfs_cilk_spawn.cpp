#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <sys/timeb.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>

#include "shared.h"
const char *algorithm_name = "bfs_cilk_spawn";

void run(int num_threads, int no_of_nodes, bool* h_graph_mask, Node* h_graph_nodes, bool* h_graph_visited, int* h_cost, bool* h_updating_graph_mask, int *h_graph_edges)
{
    char cilk_nworkers[3];
    sprintf(cilk_nworkers, "%d", num_threads);
    __cilkrts_set_param("nworkers", cilk_nworkers);

    bool keep_going;
    do
    {
        //if no thread changes this value then the loop stops
        keep_going=false;

        for (int i=0; i<num_threads; i++)
        {
            int start, end;
            dist(i, num_threads, no_of_nodes, &start, &end);
            cilk_spawn phase_one(start, end, h_graph_mask, h_graph_nodes, h_graph_visited, h_cost, h_updating_graph_mask, h_graph_edges);
        }
        cilk_sync;

        for (int i=0; i<num_threads; i++)
        {
            int start, end;
            dist(i, num_threads, no_of_nodes, &start, &end);
            cilk_spawn phase_two(start, end, &keep_going, h_graph_mask, h_graph_visited, h_updating_graph_mask);
        }
        cilk_sync;
    }
    while(keep_going);
}